package akun;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SwingConstants;

public class loginFrame extends Akun {

	private JFrame frame;
	private JTextField txtUsername;
	private JPasswordField txtPasword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					loginFrame window = new loginFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public loginFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 633, 475);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(77, 144, 79, 23);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblPasword = new JLabel("Pasword");
		lblPasword.setBounds(77, 214, 69, 20);
		frame.getContentPane().add(lblPasword);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(204, 141, 146, 26);
		frame.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		txtPasword = new JPasswordField();
		txtPasword.setBounds(204, 211, 146, 23);
		frame.getContentPane().add(txtPasword);
		
		JButton btnLogin = new JButton("Log In");
		btnLogin.setBounds(204, 286, 115, 29);
		frame.getContentPane().add(btnLogin);
		btnLogin.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String username = txtUsername.getText();
				String pasword = txtPasword.getText();
				Akun akun =new Akun();
				
				if (username.equals(akun.getUserName()) && pasword.equals(akun.getPasword()) ) {
					txtUsername.setText(null);
					txtPasword.setText(null);
					
				}else {
					JOptionPane.showMessageDialog(null, "invalid data", "Login Error", JOptionPane.ERROR_MESSAGE);
					txtUsername.setText(null);
					txtPasword.setText(null);
				}
				
			}
		});
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtUsername.setText(null);
				txtPasword.setText(null);
			}
		});
		btnReset.setBounds(470, 286, 115, 29);
		frame.getContentPane().add(btnReset);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.setBackground(Color.RED);
		btnExit.setBounds(470, 350, 115, 29);
		frame.getContentPane().add(btnExit);
		
		JLabel lblMessenger = new JLabel("Messenger");
		lblMessenger.setHorizontalAlignment(SwingConstants.CENTER);
		lblMessenger.setForeground(new Color(0, 0, 0));
		lblMessenger.setBounds(204, 16, 167, 26);
		frame.getContentPane().add(lblMessenger);
	}
}
